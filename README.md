# Niv Scanner library
## Instalación

* Incluir en la carpeta Libs el archivo nivscanner-debug.aar que se encuentra en el directorio app/libs: 
  * _[carpeta_proyecto]/app/libs_
* Incluir en el archivo build.gradle del directorio app: 
   * _implementation fileTree(dir: 'libs', include: ['*.jar', '*.aar'])_
* Sincronizar proyecto con archivos gradle:
   *  _Menú File>Sync Gradle with project files_

## Uso

- Implementar la clase ScannerListener
- Iniciar el escaner:
   -_ScannerUHF.initScanner();_
- Lanzar un escaneo con la contraseña del tag y el listener
    - _ScannerUHF.readUserMemory(passString, this);_  
    - Por defecto la contraseña suele ser "00000000"
- El resultado se devuelve en un hilo secundario, por lo que se debe traer al principal
    - _runOnUiThread()_
- Puedes ver un ejemplo de uso en la clase UHFActivity de este mismo proyecto.