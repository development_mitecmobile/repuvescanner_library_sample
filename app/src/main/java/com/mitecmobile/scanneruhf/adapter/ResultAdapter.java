package com.mitecmobile.scanneruhf.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mitecmobile.scanneruhf.R;

import java.util.List;

import es.movion.repuve_utils.data.RepuveTag;

public class ResultAdapter extends ArrayAdapter<RepuveTag> {

    private List<RepuveTag> datos;

    public ResultAdapter(Context context, List<RepuveTag> datos) {
        super(context, R.layout.row_resultados, datos);
        this.datos=datos;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.row_resultados, null);

        TextView lblRepuve = (TextView)item.findViewById(R.id.row_repuve);
        lblRepuve.setText(datos.get(position).getRepuve());

        TextView lblNiv = (TextView)item.findViewById(R.id.row_niv);
        lblNiv.setText(datos.get(position).getNiv());

        return(item);
    }
}
