package com.mitecmobile.scanneruhf.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.mitecmobile.scanneruhf.R;

import java.util.ArrayList;
import java.util.List;

public class BarcodeActivity extends BaseScannerActivity{
    public static final String INTENT_ACTION = "es.movion.barcode";
    public static final String INTENT_EXTRA = "scanResult";
    private List<String> resultados;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        resultados = new ArrayList();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, resultados);
        ListView listaResultados = findViewById(R.id.listaResultados);
        listaResultados.setAdapter(adapter);
        registerReceiver(barcodeReceiver, getIntentFilter());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(barcodeReceiver);
    }

    public static IntentFilter getIntentFilter(){
        return new IntentFilter(INTENT_ACTION);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.menu_clean == item.getItemId()) {
            clearScreen(resultados, adapter);
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    public void addResultado(String resultado){
        runOnUiThread(() -> {
            if(resultados.contains(resultado))
                return;
            resultados.add(resultado);
            adapter.notifyDataSetChanged();
        });
    }

    private BroadcastReceiver barcodeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(!INTENT_ACTION.equals(intent.getAction()))
                return;
            String barcode = intent.getStringExtra("scanResult");
            addResultado(barcode);
        }
    };
}
