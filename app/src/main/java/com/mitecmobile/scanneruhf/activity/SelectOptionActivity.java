package com.mitecmobile.scanneruhf.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.mitecmobile.scanneruhf.R;

public class SelectOptionActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_option);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btnUhf = (Button) findViewById(R.id.btnUhf);
        Button btnBarcode = (Button) findViewById(R.id.btnBarcode);
        btnUhf.setOnClickListener(this);
        btnBarcode.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (R.id.btnUhf == view.getId())
            startActivity(new Intent(this, UHFActivity.class));
        if (R.id.btnBarcode == view.getId())
            startActivity(new Intent(this, BarcodeActivity.class));
    }
}
