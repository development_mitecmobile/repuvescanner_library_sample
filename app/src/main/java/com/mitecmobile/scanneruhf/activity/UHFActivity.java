package com.mitecmobile.scanneruhf.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mitecmobile.scanneruhf.R;
import com.mitecmobile.scanneruhf.adapter.ResultAdapter;

import java.util.ArrayList;
import java.util.List;

import es.movion.nivscanner.UhfScanner;
import es.movion.nivscanner.exception.HardwareNotFoundException;
import es.movion.nivscanner.exception.InvalidPasswordException;
import es.movion.nivscanner.exception.InvalidPowerException;
import es.movion.nivscanner.listener.ScannerUHFListener;
import es.movion.repuve_utils.data.RepuveTag;

public class UHFActivity extends BaseScannerActivity implements ScannerUHFListener, SeekBar.OnSeekBarChangeListener {

    private Handler handler;
    private List<RepuveTag> resultados;
    private ResultAdapter adapter;
    private TextView txtPower;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uhf);

        ListView listaResultados = (ListView) findViewById(R.id.listaResultados);
        resultados = new ArrayList();
        adapter = new ResultAdapter(this, resultados);
        listaResultados.setAdapter(adapter);

        SeekBar seekBar = (SeekBar) findViewById(R.id.sbPower);
        seekBar.setOnSeekBarChangeListener(this);

        txtPower = (TextView) findViewById(R.id.txtPower);
        txtPower.setText(String.valueOf(seekBar.getProgress()));

        handler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            UhfScanner.initScanner();
        } catch (HardwareNotFoundException e) {
            logException(e);
        }
    }

    @Override
    protected void onPause() {
        UhfScanner.release();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.menu_clean == item.getItemId()) {
            clearScreen(resultados, adapter);
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isScanKey(keyCode)) {
            scan();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void scan() {
        try {
            UhfScanner.readUserMemory(this);
        } catch (InvalidPasswordException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void processError(final String output) {
        handler.post(() -> Toast.makeText(UHFActivity.this, output, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void processResult(final RepuveTag output) {
        handler.post(() -> {
            if (!resultados.contains(output)) {
                resultados.add(output);
                adapter.notifyDataSetChanged();
            } else {
                Toast.makeText(UHFActivity.this, R.string.error_tag_leida, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        txtPower.setText(String.valueOf(i));
        try {
            UhfScanner.setPower(i);
        } catch (InvalidPowerException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
