package com.mitecmobile.scanneruhf.activity;

import android.view.KeyEvent;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mitecmobile.scanneruhf.R;

import java.util.List;

public class BaseScannerActivity extends AppCompatActivity {

    protected void logException(Exception e) {
        Toast.makeText(this, R.string.error_hardware_not_found, Toast.LENGTH_LONG).show();
        e.printStackTrace();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    protected void clearScreen(List lista, ArrayAdapter adapter) {
        lista.clear();
        adapter.notifyDataSetChanged();
    }

    protected boolean isScanKey(int keyCode) {
        return KeyEvent.KEYCODE_WAKEUP == keyCode ||
                KeyEvent.KEYCODE_F1 == keyCode ||
                KeyEvent.KEYCODE_F2 == keyCode ||
                421 == keyCode ||
                422 == keyCode;
    }
}
